export type HousingType = 'ind' | 'col' | 'other'

export interface APIDetailUser {
  CodeINSEE: number;
  createdAt: string;
  email: string;
  emailConfirmed: boolean;
  id: number;
  isAdmin: boolean;
  peopleNumber: number;
  primaryResidence: HousingType;
  surface: number;
  updatedAt: string;
}

export interface User extends Omit<APIDetailUser, 'createdAt' | 'updatedAt'> {
  createdAt: Date;
  updatedAt: Date;
}

export interface LoginAPI {
  jwt: string;
  user: APIDetailUser;
}

