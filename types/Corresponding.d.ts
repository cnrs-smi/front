import dayjs from 'dayjs'

export interface CorrespondingAPIDetail {
  createdAt: string;
  electricity: number;
  gas: number;
  id: number;
  waterCO2: number;
  waterElec: number;
}

export type CorrespondingCreation = Omit<CorrespondingAPIDetail, 'createdAt' | 'id'>

export interface Corresponding extends Omit<CorrespondingAPIDetail, 'createdAt'> {
  createdAt: dayjs.Dayjs;
}

