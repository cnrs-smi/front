import type { Dayjs } from 'dayjs'
import type { ListMeta } from '#/index'

export interface Consumption {
  createdAt: Dayjs;
  electricity: number | null;
  gas: number | null;
  id: number;
  updatedAt: Dayjs;
  water: number | null;
}

export interface ConsumptionList extends Consumption {
  electricityCO2: number;
  gasCO2: number;
  waterCO2: number;
}

export interface ConsumptionAPIList {
  data: Array<Omit<ConsumptionList, 'createdAt' | 'updatedAt'> & { createdAt: string; updatedAt: string; }>;
  meta: ListMeta;
}

export type ConsumptionCreation = Pick<Consumption, 'electricity' | 'gas' | 'water'>
