export interface Typology {
  CodeINSEE: string;
  Type_de_territoires: 'Commune multipolarisée' | 'Pôle secondaire' | 'Rural isolé' | 'Commune périurbaine' | 'Urbain dense';
}
