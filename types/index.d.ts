export interface APIError {
  message: string;
  status: number;
  codeError?: string;
}

export interface OpenDataRecord {
  datasetid: string;
  recordid: string;
  fields: {
    geo_point_2d: [number, number];
    population: number
    nom_region: string;
    superficie: number;
    nom_comm: string;
    geo_shape: {
      coordinates: [number, number][][];
      type: string;
    },
    statut: string;
    code_reg: string;
    code_comm: string;
    code_dept: string;
    code_arr: string;
    postal_code: string;
    nom_dept: string;
    insee_com: string;
    id_geofla: string;
    code_cant: string;
    z_moyen: number;
  };
  geometry: {
    type: string;
    coordinates: [number, number];
  };
  record_timestamp: string;
}

export interface OpenDataResponse {
  nhits: number;
  parameters: {
    dataset: string;
    q: string;
    rows: number;
    start: number;
    format: string;
    timezone: string;
  };
  records: OpenDataRecord[];
}

export interface ListMeta {
  count?: number;
  limit?: number;
  page?: number;
}

export interface QTableOnRequestEvent {
  /**
   * Pagination object
   */
  pagination: {
    /**
     * Column name (from column definition)
     */
    sortBy: string;
    /**
     * Is sorting in descending order?
     */
    descending: boolean;
    /**
     * Page number (1-based)
     */
    page: number;
    /**
     * How many rows per page? 0 means Infinite
     */
    rowsPerPage: number;
    /**
     * For server-side fetching only. How many total database rows are there to be added to the table.
     */
    rowsNumber: number;
  };
  /**
   * Filter method (the 'filter-method' prop)
   * @param rows Array of rows
   * @param terms Terms to filter with (is essentially the 'filter' prop value)
   * @param cols Optional column definitions
   * @param getCellValue Optional function to get a cell value
   * @returns Filtered rows
   */
  filter: (
    rows: never[],
    terms: string | never,
    cols?: never[],
    getCellValue?: (col: never, row: never) => never,
  ) => never[];
  /**
   * Function to get a cell value
   * @param col Column name from column definitions
   * @param row The row object
   * @returns Parsed/Processed cell value
   */
  getCellValue: (col: never, row: never) => never;
}
