import { afterEach, beforeEach, describe, expect, test, vi } from 'vitest'
import { initLocalStorage } from '~/mocks/storage.mock'
import { jwt } from '~/fixtures/security.fixtures'
import { useSecurityStore } from '@/store/useSecurityStore'
import { createPinia, setActivePinia } from 'pinia'
import _axios from '@/plugins/axios'

describe('useSecurityStore', () => {

  let axiosGetSpy = vi.spyOn(_axios, 'get')
  beforeEach(() => {
    axiosGetSpy = vi.spyOn(_axios, 'get')
    initLocalStorage()
    localStorage.setItem('Jwt', jwt)
    setActivePinia(createPinia())
  })

  afterEach(() => {
    vi.resetAllMocks()
  })

  test('Should load Jwt from localstorage', () => {
    const pinia = useSecurityStore()
    expect(pinia.jwt).toBe(null)
    pinia.initJWT()
    expect(pinia.jwt).toBe(jwt)
  })

  test('Should test if user is connected', () => {
    const pinia = useSecurityStore()
    expect(pinia.userIsConnected).toBeFalsy()
    pinia.initJWT()
    expect(pinia.userIsConnected).toBeTruthy()
  })

  test('Should test if there\'s no jwt in state userId is null', () => {
    const pinia = useSecurityStore()
    expect(pinia.userId).toBeNull()
  })
})
