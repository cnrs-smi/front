import { defineStore } from 'pinia'
import { Consumption, ConsumptionAPIList, ConsumptionCreation } from '#/Consumption'
import _axios from '@/plugins/axios'
import dayjs from 'dayjs'
import { ListMeta } from '#/index'

export interface ConsumptionStoreState {
  consumptions: Consumption[];
  consumptionPagination: ListMeta;
}

export const useConsumptionStore = defineStore('Consumption', {
  state: (): ConsumptionStoreState => ({
    consumptions: [],
    consumptionPagination: {
      count: 0,
      limit: 10,
      page: 1,
    },
  }),

  actions: {
    /**
     * It takes a new consumption object, sends it to the server, and then reloads the consumptions from the server
     * @param {ConsumptionCreation} newConsumption - ConsumptionCreation
     */
    async addConsumption(newConsumption: ConsumptionCreation): Promise<void> {
      await _axios.post('/api/consumptions', newConsumption, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      })
      await this.loadConsumptions()
    },
    /**
     * It updates a consumption in the database, then reloads the consumptions from the database
     */
    async loadConsumptions(): Promise<void> {
      const response = await _axios.get<ConsumptionAPIList>(
        `/api/consumptions?limit=${this.consumptionPagination.limit}&page=${this.consumptionPagination.page}`,
        {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
        })

      this.consumptionPagination = response.data.meta

      this.consumptions = response.data.data.map((consumption) => ({
        ...consumption,
        createdAt: dayjs(consumption.createdAt),
        updatedAt: dayjs(consumption.updatedAt),
      }))
    },
    /**
     * If the new value is different from the old value,
     * it updates a consumption in the database and reloads the consumptions from the database.
     * @param {'electricity' | 'gas' | 'water'} fieldUpdated - 'electricity' | 'gas' | 'water',
     * @param {number} newValue - number,
     * @param {number} id - the id of the consumption to update
     */
    async updateConsumption(
      fieldUpdated: 'electricity' | 'gas' | 'water',
      newValue: number,
      id: number,
    ): Promise<void> {
      const consumptionUpdated = this.consumptions.find((consumption) => consumption.id === id)

      if (!newValue && consumptionUpdated && consumptionUpdated[fieldUpdated] !== newValue) {
        await _axios.patch<Consumption>(`/api/consumptions/${id}`, {
          [fieldUpdated]: newValue,
        }, {
          headers: {
            Authorization: `Bearer ${localStorage.getItem('token')}`,
          },
        })
        await this.loadConsumptions()
      }
    },
  },
})
