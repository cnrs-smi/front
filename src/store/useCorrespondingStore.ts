import { defineStore } from 'pinia'
import type { Corresponding, CorrespondingAPIDetail, CorrespondingCreation } from '#/Corresponding'
import _axios from '@/plugins/axios'
import dayjs from 'dayjs'

interface CorrespondingStoreState {
  corresponding: Corresponding[]
}

export const useCorrespondingStore = defineStore('Corresponding', {
  state: (): CorrespondingStoreState => ({
    corresponding: [],
  }),

  actions: {
    async createCorresponding(corresponding: CorrespondingCreation): Promise<void> {
      await _axios.post<CorrespondingAPIDetail>('/api/corresponding', corresponding, {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      })
      await this.loadCorrespondingList()
    },
    async loadCorrespondingList(): Promise<Corresponding[]> {
      this.corresponding = (await _axios.get<CorrespondingAPIDetail[]>('/api/corresponding', {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      })).data.map(corresponding => ({
        ...corresponding,
        createdAt: dayjs(corresponding.createdAt),
      }))

      return this.corresponding
    },
  },
})
