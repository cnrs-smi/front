import { defineStore } from 'pinia'
import { User } from '#/Users'
import _axios from '@/plugins/axios'

export interface UserStateStore {
  users: User[];
}

export const useUserStore = defineStore('User', {
  state: (): UserStateStore => ({
    users: [],
  }),
  actions: {
    async loadUsersByMail(email: string): Promise<User[]> {
      this.users = (await _axios.get<User[]>('/api/users/' + email, {
        headers: {
          Authorization: 'Bearer ' + localStorage.getItem('token'),
        },
      })).data
      return this.users
    },
  },
})
