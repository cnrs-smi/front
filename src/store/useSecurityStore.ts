import { defineStore } from 'pinia'
import { User } from '#/Users'
import _axios from '@/plugins/axios'

export interface SecurityStoreState {
  jwt: string | null;
  me: User | null;
}

export interface JWTBody {
  email: string;
  exp: number;
  iat: number;
  id: number;
}


export const useSecurityStore = defineStore('Security', {
  state: (): SecurityStoreState => ({
    jwt: null,
    me: null,
  }),

  getters: {
    isAdmin: (state): boolean => state.me?.isAdmin || false,
    userIsConnected: (state): boolean => !!state.jwt,
    userId: (state): number | null => {
      if (state.jwt === null) return null
      return (JSON.parse(window.atob(state.jwt.split('.')[1])) as JWTBody).id
    },
  },


  actions: {
    initJWT(): void {
      this.jwt = localStorage.getItem('token') || null
    },
    async loadMe(): Promise<User> {
      if (this.me) return this.me

      const response = await _axios.get<User>('/me', {
        headers: {
          Authorization: `Bearer ${localStorage.getItem('token')}`,
        },
      })
      this.me = response.data
      return response.data
    },
  },
})
