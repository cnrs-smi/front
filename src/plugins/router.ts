import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router'
import { useSecurityStore } from '@/store/useSecurityStore'

const routes: RouteRecordRaw[] = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/HomeView.vue'),
  },
  {
    path: '/nos-gestes-climats',
    name: 'NGC',
    component: () => import('@/views/NGC.vue'),
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('@/views/Authentication/Register.vue'),
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/Authentication/Login.vue'),
    meta: {
      noAuth: true,
    },
  },
  {
    path: '/email-confirmation',
    name: 'EmailConfirmation',
    component: () => import('@/views/Authentication/MailConfirmation.vue'),
  },
  {
    path: '/forgotten-password',
    name: 'ForgottenPassword',
    component: () => import('@/views/Authentication/ForgottenPassword.vue'),
  },
  {
    path: '/reset-password',
    name: 'ResetPassword',
    component: () => import('@/views/Authentication/ResetPassword.vue'),
  },
  {
    path: '/dashboard',
    name: 'Dashboard',
    component: () => import('@/views/DashboardView.vue'),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/consumption',
    name: 'Consumption',
    component: () => import('@/views/ConsumptionView.vue'),
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: '/settings',
    name: 'Settings',
    component: () => import('@/views/SettingsView.vue'),
    meta: {
      requiresAuth: true,
      admin: true,
    },
  },
  {
    path: '/charts',
    name: 'Charts',
    component: () => import('@/views/ChartsView.vue'),
    children: [
      {
        path: 'day',
        name: 'ChartsDay',
        component: () => import('@/views/Charts/DailyCharts.vue'),
      },
      {
        path: 'month',
        name: 'ChartsMonth',
        component: () => import('@/views/Charts/MonthlyCharts.vue'),
      },
      {
        path: 'year',
        name: 'ChartsYear',
        component: () => import('@/views/Charts/YearlyCharts.vue'),
      },
    ],
  },
  {
    path: '/error',
    name: 'Error',
    component: () => import('@/views/Errors/ErrorTemplate.vue'),
    children: [
      {
        path: '403',
        name: 'Error403',
        component: () => import('@/views/Errors/Error403.vue'),
      },
    ],
  },
]

const router = createRouter({
  history: createWebHistory(),
  routes,
})

router.beforeEach(async (to, from, next) => {
  const securityStore = useSecurityStore()
  securityStore.initJWT()

  if (to.meta.noAuth && securityStore.userIsConnected) {
    next({ name: 'Dashboard' })
  } else if (to.meta.requiresAuth) {
    if (!securityStore.userIsConnected) {
      next({ name: 'Login' })
    } else if (to.meta.admin) {
      await securityStore.loadMe()
      if (!securityStore.isAdmin) {
        next({ name: 'Error403' })
      } else {
        next()
      }
    } else {
      next()
    }
  } else {
    next()
  }
})

export default router
