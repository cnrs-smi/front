import { createApp } from 'vue'
import App from './App.vue'

// Quasar
import { Loading, Notify, Quasar } from 'quasar'
import quasarLang from 'quasar/lang/fr'
import quasarIconSet from 'quasar/icon-set/svg-mdi-v6'

// Import icon libraries
import '@quasar/extras/roboto-font/roboto-font.css'
import '@quasar/extras/mdi-v6/mdi-v6.css'

// Import Quasar css
import 'quasar/src/css/index.sass'

import i18n from '@/plugins/i18n'
import router from '@/plugins/router'
import { createPinia } from 'pinia'

import dayjs from 'dayjs'
import 'dayjs/locale/fr'
import 'dayjs/locale/en'
import localizedFormat from 'dayjs/plugin/localizedFormat'

dayjs.extend(localizedFormat)
dayjs.locale(i18n.global.locale)

createApp(App)
  .use(i18n)
  .use(router)
  .use(createPinia())
  .use(Quasar, {
    plugins: {
      Loading,
      Notify,
    }, // import Quasar plugins and add here
    lang: quasarLang,
    iconSet: quasarIconSet,
    config: {
      loading: {},
      notify: {},
    },
  })
  .mount('#app')
